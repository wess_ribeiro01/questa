<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
    	for ($i=0; $i < 20; $i++) { 
        	$Category = new \questa\Category();
        	$Category->nome = "$i.nome categoria";
        	$Category->id_language = rand(1,3);
        	$Category->save();
    	}
    }
}
