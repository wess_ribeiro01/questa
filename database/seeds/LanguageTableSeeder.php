<?php

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    public function run()
    {
    	for ($i=1; $i <= 3; $i++) { 
        	$language = new \questa\Language();
        	if ($i == 1) {
        		$language->nome = "Português";
        	}else if($i == 2){
        		$language->nome = "English";
        	}else{
        		$language->nome = "Español";
        	}
        	$language->save();
    	}
    }
}
