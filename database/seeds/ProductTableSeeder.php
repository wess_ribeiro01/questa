<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i=0; $i < 20; $i++) { 
	        $Product = new \questa\Products;
	    	$Product->nome = "$i.nome produto";
	    	$Product->vantagens = "$i.vantagens produto";
	    	$Product->caracteristicas = "$i.caracteristicas produto";
	    	$Product->id_language = rand(1,3);
	    	$Product->id_category = rand(1,20);
	    	$Product->flag_eua = rand(0,1);
	    	$Product->flag_br = rand(0,1);
	    	$Product->save();
	    }
    }
}
