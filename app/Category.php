<?php

namespace questa;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;

class Category extends Model
{
	protected $table = 'categories';
	protected $dates = ['deleted_at'];
	protected $primaryKey = 'id';

}
