<?php

namespace questa;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';
	protected $primaryKey = 'id';
}
