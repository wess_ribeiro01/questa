<?php

namespace questa;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;

class Language extends Model
{
    protected $table = 'languages';
	protected $dates = ['deleted_at'];
	protected $primaryKey = 'id';

}
