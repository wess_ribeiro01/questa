<?php

namespace questa\Http\Controllers;
use Illuminate\Support\Facades\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use \questa\Category;
use \questa\Language;

class CategoryController extends Controller
{
    public function index()
    {
    	$categories = \questa\Category::paginate(10);
    	$languages = \questa\Language::all();
        return view('category.index')->with(['categories' => $categories, 'languages' => $languages]);
    }
}
