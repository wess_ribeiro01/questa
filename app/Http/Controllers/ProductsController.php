<?php

namespace questa\Http\Controllers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use \questa\Products;
class ProductsController extends Controller
{
    public function index()
    {
    	$products = \questa\Products::paginate(10);
        return view('product.index')->with('products', $products);
    }
}
