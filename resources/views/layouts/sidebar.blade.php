<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ url('categorias') }}"><i class="fa fa-list"></i> Categorias</span></a>
            </li>
            <li>
                <a href="{{ url('produtos') }}"><i class="fa fa-shopping-cart"></i> Produtos</span></a>
            </li>
        </ul>
    </div>
</div> 

