@extends('layouts.master')
@section('title') Categorias @endsection
@section('content')
	<div class="col-lg-12">
	    <h1 class="page-header"><i class="fa fa-list" ></i>&nbsp;Categorias</h1>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<form class="well">
				<div class="form-group">
                    <label>Nome*</label>
                    <input class="form-control" placeholder="Nome">
                </div>
				<div class="form-group">
		            <label>Idiomas*</label>
		            <select class="form-control">
	                    <option>Selecione...</option>
	                    @foreach ($languages as $lang)
	                    	<option>{{$lang->nome}}</option>
	                    @endforeach
	                </select>
		        </div>
			</form>
		</div>
	</div>
	<table class="table table-bordered table-striped table-hover" id="gridCategorias">
		<thead>
			<tr>
				<th>
					Id
				</th>
				<th>
					Nome
				</th>
				<th>
					Ações
				</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($categories as $c)
				<tr>
					<td>
						{{ $c->id }}
					</td>
					<td>
						{{ $c->nome }}
					</td>
					<td>
						<a href="" class="btn btn-primary ">Editar</a>
						<a href="" class="btn btn-danger ">Deletar</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	
	<div class="row">
		<div class="col-md-12 text-center">
			{{$categories->links()}}
		</div>
	</div>
@endsection