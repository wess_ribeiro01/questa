@extends('layouts.master')
@section('title') Produtos @endsection
@section('content')
	<div class="col-lg-12">
	    <h1 class="page-header"><i class="fa fa-shopping-cart"></i>&nbsp;Produtos</h1>
	</div>
	<table class="table table-bordered table-striped table-hover" id="gridProdutos">
		
		<tbody>
			@foreach ($products as $p)
				<tr>
					<td>
						{{ $p->nome }}
					</td>
					<td>
						<a href="" class="btn btn-primary ">Editar</a>
						<a href="" class="btn btn-danger ">Deletar</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	
	<div class="row">
		<div class="col-md-12 text-center">
			{{$products->links()}}
		</div>
	</div>
@endsection